<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;

/**
 * Class ContentCategoryModel
 * @package App\Models
 *
 * @property string $title
 * @property string $slug
 * @property int $category_id
 * @property string $intro
 * @property string $content
 * @property string $image
 * @property string $material
 * @property string $guarantee
 * @property int $price
 * @property int $discount
 * @property int $is_lock
 */
class AreaModel extends BaseModel
{
    protected $table = 'area';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;

    protected $allowedFields = ['name'];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';
    protected $dateFormat = 'int';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    public function getRules(string $scenario = null): array
    {
        return [
            'name' => 'required|min_length[3]|max_length[255]',
        ];
    }
    public function select_area($id){
        return $this->db->query('SELECT a.`id`, a.`name` FROM `area` a 
                            JOIN administrator am ON am.`area_id` = a.`id`
                            WHERE am.`id` = ?',[$id])->getRow();
    }
}
