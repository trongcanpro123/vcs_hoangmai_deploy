<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;

/**
 * Class ContentCategoryModel
 * @package App\Models
 *
 * @property int $car_number
 * @property string $note
 */
class MaterialModel extends BaseModel
{
    protected $table = 'material_types_config';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;
    protected $allowedFields = ['material_name', 'material_code'];
    public function select_metarial_id($id)
    {
        return $this->db->query('SELECT * FROM material_types_config WHERE id = ?', [$id])->getRow();
    }
    public function select_metarial_id_arr($id)
    {
        return $this->db->query('SELECT * FROM material_types_config WHERE id = ?', [$id])->getResultArray();
    }
     /**
     * @param string|null $scenario
     * @return array
     */
    public function getRules(string $scenario = null): array
    {
        return [
    
        ];
    }
}