<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectCategoryModel $model
 * @var \CodeIgniter\Validation\Validation $validator
 */
$this->title = 'Cập nhật xe';
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-info flex-align">
                <div>
                    <h4 class="card-title"><?= $this->title ?></h4>
                </div>
            </div>
            <div class="card-body">
                <form action="<?= route_to('admin_car_update', $model->getPrimaryKey()) ?>" method="post"
                      enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                            <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Biển số xe</label>
                            <?= Html::textInput('car_number', $model->car_number, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                // 'disabled'=> $readonly ? true : false,
                                'autofocus' => true,
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Tên tài xế</label>
                            <?= Html::textInput('drive_name', $model->drive_name, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                // 'disabled'=> $readonly ? true : false,
                                'autofocus' => true,
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Loại xe</label>
                            <?= Html::textInput('car_type', $model->car_type, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                // 'disabled'=> $readonly ? true : false,
                                'autofocus' => true,
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Đơn vị</label>
                            <?= Html::textInput('delivery_unit', $model->delivery_unit, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                // 'disabled'=> $readonly ? true : false,
                                'autofocus' => true,
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="bmd-label-floating">Lưu ý</label>
                            <?= Html::textInput('note', $model->note, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                // 'disabled'=> $readonly ? true : false,
                                'autofocus' => true,
                            ]) ?>
                        </div>
                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <a href="<?= route_to('bv_list_cars') ?>" class="btn btn-round">Huỷ</a>
                                            <button style="margin-left:15px" class="btn btn-success btn-round"
                                                    type="submit">Lưu
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ($validator): ?>
                            <div class="alert alert-danger" style="margin-top: 32px;">
                                <ul style="margin: 0; padding-left: 16px;">
                                    <?php foreach ($validator->getErrors() as $error): ?>
                                        <li><?= Html::decode($error) ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
            </div>
        </div>
    </div>