<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Kiểm soát vào ra';
?>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="margin-bottom: 0px">
                    <div class="card-header card-header-text card-header-info">
                        <div class="card-text">
                            <h6 class="card-title">Chọn Kho</h6>
                        </div>
                    </div>

                    <div class="card-body" style="padding-top: 0px">
                        <div class="row">
                            <div class="col-md-4 text-right">

                                <select class="form-control " data-style="btn btn-link" id="area-select">
                                    <?php
                                    $current_user = (\App\Models\AdministratorModel::findIdentity());
                                    if ($area) {
                                        foreach ($area as $item) {
                                            if($current_user->type_user == 'lanh_dao' || $current_user->area_id == $item->id){
                                                ?>
                                                <option value="<?= $item->id ?>" ><?= $item->name ?></option>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-2 text-right">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" id="check_cam_in" name="check_cam_in" type="checkbox" checked onchange="change_check_cam_in();" >
                                        Camera Cổng Vào
                                        <span class="form-check-sign"><span class="check"></span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="">
                                    <select class="form-control " placeholder="Chọn Camera Vào" data-style="btn btn-link" id="select_camera_in" onchange="change_play_cam_gate_in(this, 'videoElement_in')">
                                        <?php if($camera_in) {
                                            foreach ($camera_in as $camera){
                                                ?>
                                                <option value="<?= $camera->get_link_play() ?>" ><?= $camera->title ?></option>

                                                <?php
                                            }
                                        } ?>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 text-right">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" id="check_cam_out"  name="check_cam_out" type="checkbox" checked onchange="change_check_cam_out()" >
                                        Camera Cổng Ra
                                        <span class="form-check-sign"><span class="check"></span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control " placeholder="Chọn Camera Ra" data-style="btn btn-link" id="select_camera_out" onchange="change_play_cam_gate_out(this, 'videoElement_out')">
                                    <?php if($camera_out) {
                                        foreach ($camera_out as $camera){
                                            ?>
                                            <option value="<?= $camera->get_link_play() ?>" ><?= $camera->title ?></option>

                                            <?php
                                        }
                                    } ?>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 div_camera_in ">
                <div class="card" style="margin-top: 20px">
                    <div class="card-body text-center">
                        <video width="100%" id="videoElement_in"></video>
                        <canvas style="display: none" id="canvas_videoElement_in" ></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-6 div_camera_out">
                <div class="card" style="margin-top: 20px">
                    <div class="card-body text-center">
                        <video width="100% text-center" id="videoElement_out"></video>
                        <canvas style="display: none" id="canvas_videoElement_out" ></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    // window.onload = function() {
    //     change_play_cam_gate_in($('#select_camera_in'), 'videoElement_in');
    //     change_play_cam_gate_out($('#select_camera_out'), 'videoElement_out');
    // };

    window.onload = function() {

        change_play_cam_gate_in($('#select_camera_in'), 'videoElement_in');
        change_play_cam_gate_out($('#select_camera_out'), 'videoElement_out');

        document.addEventListener("visibilitychange", visibilitychange);
    };

    function refresh_cam(){
        change_play_cam_gate_in($('#select_camera_in'), 'videoElement_in');
        change_play_cam_gate_out($('#select_camera_out'), 'videoElement_out');
    }
    function visibilitychange (evt) {

        if(is_on_change) return;
        console.log('refresh');
        is_on_change = true;
        refresh_cam();
        is_on_change = false;
    }
</script>