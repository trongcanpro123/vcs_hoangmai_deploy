<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\AdministratorModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Thứ tự lấy ticket';
?>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-text card-header-info">
                        <div class="card-icon">
                            <i class="material-icons">search</i>
                        </div>

                    </div>
                    <div class="card-body ">
                        <form method="get">
                            <div class="row">
                                <div class="col-md-3">
                                    <input name="driver_name" class="form-control" type="search"
                                           placeholder="Tên tài xế"
                                           aria-label="Search"
                                           value="<?= $param_search ? $param_search['driver_name'] : '' ?>">
                                </div>
                                <div class="col-md-3">
                                    <div class='input-group date'>
                                        <input type='text' name="car_number" placeholder="Biển số"
                                               class="form-control " autocomplete="off"
                                               value="<?= $param_search ? $param_search['car_number'] : '' ?>">
                                    </div>
                                </div>

                                <div class="col-md-6 text-center">
                                    <button class="btn btn-info btn-round" type="submit">Tìm kiếm</button>
                                    <a style="color: white" class="btn btn-rose btn-round" target="_blank"
                                       href="/thong-bao-nhan-ticket/<?= $area_id ?>">Màn hình thông báo</a>

                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header card-header-info flex-align">
                        <div>
                            <h4 class="card-title">Thứ tự lấy Ticket</h4>
                        </div>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tài xế</th>
                                <th>Biển số</th>
                                <th>Thời gian vào</th>
                                <th class="text-center">Thực hiện</th>

                            </tr>
                            </thead>
                            <tbody id="myTable">

                            <?php
                            if ($models): ?>
                                <?php foreach ($models as $n => $model): ?>
                                    <tr>
                                        <td><?= $model->checkin_order ?></td>
                                        <td><?= $model->driver_name ?></td>
                                        <td><?= $model->car_number ?></td>

                                        <td><?= date('d-m-Y H:i:s', strtotime($model->checkin_time)) ?></td>
                                        <td class="row-actions text-right">
                                            <?php if($n == 0 || \App\Models\AdministratorModel::findIdentity()->type_user == 'lanh_dao_kho' ) {
                                                ?>
                                                <a id="btn_alert_take_ticket_<?= $model->id ?>"
                                                   class="btn btn-round btn-info btn-sm"
                                                   href="/Admin/Operator/process_alert_get_ticket/<?= $model->id ?>"> Mời
                                                    vào</a>
                                            <?php
                                            } ?>


                                            <button id="btn_reject_get_ticket_<?= $model->id ?>"
                                                    class="btn btn-round btn-danger btn-sm"
                                                    onclick="process_reject_get_ticket(<?= $model->id ?>)" value="Hủy">
                                                Hủy
                                            </button>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header card-header-primary text-center">
                        <h4 class="card-title ">Xử lý nhận ticket</h4>
                        <!--                    <p class="card-category">...</p>-->
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Tài xế</th>
                                <th>Biển số</th>
                                <th class="text-center">Thực hiện</th>
                            </tr>
                            </thead>
                            <tbody id="myTable">

                            <?php if ($list_item_wait_ticket): ?>
                                <?php foreach ($list_item_wait_ticket as $wait): ?>
                                    <tr>
                                        <td><?= $wait->driver_name ?></td>
                                        <td><?= $wait->car_number ?></td>

                                        <td class="row-actions">
                                            <button id="btn_take_ticket_<?= $wait->id ?>"
                                                    class="btn btn-round btn-success btn-sm"
                                                    onclick="process_take_ticket(<?= $wait->id ?>)" value="Ticket"> Đã
                                                xử lý
                                            </button>
                                            <a id="btn_return_back_ticket_<?= $wait->id ?>"
                                                    class="btn btn-round btn-warning btn-sm"
                                                    href="/Admin/Operator/process_return_back_ticket/<?= $wait->id ?>" value="Hủy">
                                                Xếp hàng lại
                                            </a>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>
    </div>

</div>


<script>
    window.onload = function () {

    };
</script>