<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\AdministratorModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Trợ giúp';
?>

<div class="card">
    <div class="card-header card-header-info flex-align">
        <div>
            <h4 class="card-title"><?= $this->title ?></h4>
            <p class="card-category">Dưới đây là các hướng dẫn dành cho nhân viên</p>
        </div>
    </div>
    <div class="card-body table-responsive">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <p>Bước 1: Kiểm soát thứ tự lấy ticket</p>
                        <p>Bước 2: In ticket</p>
                        <p>Bước 3: Kiểm soát thứ tự lấy hàng</p>
                        <p>Nếu có vấn đề về giàn bơm thì vui lòng vào Cấu hình tắt bật chế độ hoạt động</p>

                    </div>
                </div>
            </div>
        </div>





    </div>
</div>
