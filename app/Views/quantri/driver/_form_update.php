<style>
.image-driver{
    height:350px;
    width: 300px
}
.mr-bt {
    margin-bottom:25px;
    text-align:center;
}
.image-title {

}
.div-image {
    margin-bottom:15px;
}
.btn-capture {
    min-width:100px;
    padding: 7px 24px;
}
</style>

<?php

use App\Helpers\Html;

/**
 * @var \App\Models\DriverModel $model
 */
?>
 <?php
$current_user = (\App\Models\AdministratorModel::findIdentity());
if($current_user->type_user == 'bao_ve'){
    ?>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="margin-bottom: 0px">
                    <div class="card-header card-header-text card-header-info">
                    </div>
                    <div class="card-body" style="padding-top: 0px">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="">
                                    <select class="form-control " data-style="btn btn-link" id="select_camera_in_test">
                                        
                                                <option value="http://127.0.0.1:2003/iptv/cam1.flv" ></option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="">
                                    <select class="form-control " data-style="btn btn-link" id="select_camera_out">
                                       
                                                <option value="http://127.0.0.1:2003/iptv/cam4.flv" ></option>
                                             
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-6 div_camera_in1312324 ">
                <div class="card" style="margin-top: 20px">
                    <div class="card-body text-center">
                        <video width="100%" id="videoElement_in_test"></video>
                        <canvas style="display: none" id="canvas_videoElement_in_test" ></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-6 div_camera_out">
                <div class="card" style="margin-top: 20px">
                    <div class="card-body text-center">
                        <video width="100% text-center" id="videoElement_out"></video>
                        <canvas style="display: none" id="canvas_videoElement_out" ></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <?php
}
?>


    

<!--    right-->
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Công trình</label>
                            <?= Html::textInput('receipt', $model->construction_name, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'readOnly'=> $readonly ? true : false,
                                'autofocus' => true,
                                'value' => "PNK-20"
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Phiếu nhập kho</label>
                            <?= Html::textInput('receipt', $model->receipt, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'readOnly'=> $readonly ? true : false,
                                'autofocus' => true,
                                'value' => "PNK-20"
                            ]) ?>
                        </div>
                    </div>
                
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Địa điểm</label>
                            <?= Html::textInput('address', $model->construction_address, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'readOnly'=> true,
                                'autofocus' => true,
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Trạng thái</label>
                            <?= Html::textInput('address', $model->status, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'readOnly'=> true,
                                'color'=>"#FF0000",
                                'autofocus' => true,
                            ]) ?>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                        <label class="bmd-label-floating">Biển số xe</label>
                            <?= Html::textInput('receipt', $model->car_number, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'readOnly'=> $readonly ? true : false,
                                'autofocus' => true,
                                'value' => "PNK-20"
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <label class="bmd-label-floating">Phương pháp giao nhận</label>
                            <?= Html::textInput('receipt', $model->delivery_method, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'readOnly'=> $readonly ? true : false,
                                'autofocus' => true,
                                'value' => "PNK-20"
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                        <label class="bmd-label-floating">Đơn vị giao hàng</label>
                            <?= Html::textInput('receipt', $model->delivery_unit, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'readOnly'=> $readonly ? true : false,
                                'autofocus' => true,
                                'value' => "PNK-20"
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <label class="bmd-label-floating">Loại xe</label>
                            <?= Html::textInput('receipt', $model->car_type, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'readOnly'=> $readonly ? true : false,
                                'autofocus' => true,
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                     <div class="form-group">
                        <label class="bmd-label-floating">Loại vật liệu</label>
                            <?= Html::textInput('receipt', $model->material_name, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'readOnly'=> $readonly ? true : false,
                                'autofocus' => true,
                                'value' => "PNK-20"
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <label class="bmd-label-floating">Mã vật liệu</label>
                            <?= Html::textInput('receipt', $model->material_code, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'readOnly'=> $readonly ? true : false,
                                'autofocus' => true,
                                'value' => "PNK-20"
                            ]) ?>
                        </div>
                    </div>
                </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                        <label class="bmd-label-floating">Khối lượng nhập</label>
                            <?= Html::textInput('receipt', $model->input_volume, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'readOnly'=> $readonly ? true : false,
                                'autofocus' => true,
                                'value' => "PNK-20"
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                        <label class="bmd-label-floating">Khối lượng giảm trừ</label>
                            <?= Html::textInput('receipt', $model->reduction_volume, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'readOnly'=> $readonly ? true : false,
                                'autofocus' => true,
                                'value' => "PNK-20"
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                        <label class="bmd-label-floating">Khối lượng thực</label>
                            <?= Html::textInput('receipt', $model->actual_volume, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'readOnly'=> $readonly ? true : false,
                                'autofocus' => true,
                                'value' => "PNK-20"
                            ]) ?>
                        </div>
                    </div>
                            </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header card-header-primary text-center">
                                <h6 class="card-title">Cam phía đầu xe</h6>
                            </div>

                            <div class="card-body text-center">
                                <div class="form-group">
                                    <input type="hidden" id="checkin_image_up" name="checkin_image_up" value="<?= $model->checkin_image_up ?>"/>
                                    <img id="img_snap_left" class="image_preview_snap driver_thum" src="<?= $model->checkin_image_up ? $model->checkin_image_up : '/images/empty.jpg' ?>" />

                                </div>
                                <!-- <button onclick="snap_video('videoElement_in','img_snap_left','checkin_image_up')" class="btn-capture btn btn-primary btn-round"  type="button">Chụp</button> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header card-header-info text-center">
                                <h6 class="card-title">Cam ảnh trên thùng</h6>
                            </div>
                            <div class="card-body text-center">
                                <div class="form-group">
                                    <input type="hidden" id="checkin_image_front" name="checkin_image_front" value="<?= $model->checkin_image_front ?>"/>
                                    <img id="img_snap_front" class="image_preview_snap driver_thum"  src="<?= $model->checkin_image_front ? $model->checkin_image_front : '/images/empty.jpg' ?>" />
                                </div>
                                <!-- <button onclick="snap_video('videoElement','img_snap_front','checkin_image_front')" class="btn-capture btn btn-info btn-round"  type="button">Chụp</button> -->

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header card-header-primary text-center">
                                <h6 class="card-title">Cam ảnh thùng bên trái</h6>
                            </div>
                            <div class="card-body text-center">
                                <div class="form-group">
                                    <input type="hidden" id="checkin_image_left" name="checkin_image_left" value="<?= $model->checkin_image_left ?>"/>
                                    <img id="img_snap_right" class="image_preview_snap driver_thum"  src="<?= $model->checkin_image_left ? $model->checkin_image_left : '/images/empty.jpg' ?>" />
                                </div>
                                <!-- <button onclick="snap_video('videoElement','img_snap_right','checkin_image_left')" class="btn-capture btn btn-primary btn-round"  type="button">Chụp</button> -->


                            </div>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="card">
                            <div class="card-header card-header-primary text-center">
                                <h6 class="card-title">Cam ảnh thùng bên phải</h6>
                            </div>
                            <div class="card-body text-center">
                                <div class="form-group">
                                    <input type="hidden" id="checkin_image_right" name="checkin_image_right" value="<?= $model->checkin_image_right ?>"/>
                                    <img id="img_snap_right_test" class="image_preview_snap driver_thum"  src="<?= $model->checkin_image_right ? $model->checkin_image_right: '/images/empty.jpg' ?>" />
                                </div>
                                <!-- <button onclick="snap_video('videoElement','img_snap_right','image_right')" class="btn-capture btn btn-primary btn-round"  type="button">Chụp</button> -->


                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="card">
                            <div class="card-header card-header-primary text-center">
                                <h6 class="card-title">Cam phía đầu xe</h6>
                            </div>

                            <div class="card-body text-center">
                                <div class="form-group">
                                    <input type="hidden" id="checkin_image_up" name="checkin_image_up" value="<?= $model->checkin_image_up ?>"/>
                                    <img id="img_snap_left" class="image_preview_snap driver_thum" src="<?= $model->checkin_image_up ? $model->checkin_image_up : '/images/empty.jpg' ?>" />

                                </div>
                                <button onclick="snap_video('videoElement_in_test','img_snap_left','checkin_image_up')" class="btn-capture btn btn-primary btn-round"  type="button">Chụp</button>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header card-header-primary text-center">
                                <h6 class="card-title">Cam ảnh thùng xe ra bên trái</h6>
                            </div>
                            <div class="card-body text-center">
                                <div class="form-group">
                                    <input type="hidden" id="checkout_image_up" name="checkout_image_up" value="<?= $model->checkout_image_up ?>"/>
                                    <img id="img_snap_right1" class="image_preview_snap driver_thum"  src="<?= $model->checkout_image_up ? $model->checkout_image_up : '/images/empty.jpg' ?>" />
                                </div>
                                <?php
                                $current_user = (\App\Models\AdministratorModel::findIdentity());
                                if($current_user->type_user == 'bao_ve' && $model->status =="Đã duyệt"){
                                    ?>
                                    <button onclick="snap_video('videoElement_in_test','img_snap_right1','checkout_image_up')" class="btn-capture btn btn-primary btn-round"  type="button">Chụp</button>
                                    <?php
                                }
                                ?>
                                


                            </div>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="card">
                            <div class="card-header card-header-primary text-center">
                                <h6 class="card-title">Cam ảnh thùng xe ra bên trên</h6>
                            </div>
                            <div class="card-body text-center">
                                <div class="form-group">
                                    <input type="hidden" id="checkout_image_front" name="checkout_image_front" value="<?= $model->checkout_image_front ?>"/>
                                    <img id="img_snap_right2" class="image_preview_snap driver_thum"  src="<?= $model->checkout_image_front ? $model->checkout_image_front: '/images/empty.jpg' ?>" />
                                </div>
                                <?php
                                $current_user = (\App\Models\AdministratorModel::findIdentity());
                                if($current_user->type_user == 'bao_ve' && $model->status =="Đã duyệt"){
                                    ?>
                                    <button onclick="snap_video('videoElement_out','img_snap_right2','checkout_image_front')" class="btn-capture btn btn-primary btn-round"  type="button">Chụp</button>
                                    <?php
                                }
                                ?>


                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="<?= route_to('driver') ?>" class="btn btn-round">Trở về</a>
                        <?php
                                $current_user = (\App\Models\AdministratorModel::findIdentity());
                                if($current_user->type_user == 'nhan_vien' && $model->status =="Chờ duyệt"){
                                    ?>
                                     <button style="margin-left:15px" onclick="update_submit(<?= $model->id ?>);"class="btn btn-success btn-round"  type="button">Duyệt</button> 
                                     <button style="margin-left:15px" onclick="update_reject(<?= $model->id ?>);"class="btn btn-success btn-round"  type="button">Từ chối</button>  
                                    <?php
                                }
                                ?>
                         <?php
                                $current_user = (\App\Models\AdministratorModel::findIdentity());
                                if($current_user->type_user == 'bao_ve' && $model->checkin_image_up !="" && $model ->status == 'Đã duyệt'){
                                    ?>
                                     <!-- <button style="margin-left:15px" onclick="checkout(<?= $model->checkin_image_up ?>);"class="btn btn-success btn-round"  type="button">Check out</button> -->
                                     <button style="margin-left:15px" class="btn btn-success btn-round" type="submit">Check out</button> 
                                    <?php
                                }
                                ?>
                        <a href="#" onclick="re_print(<?= $model->id ?>);" title="In Phiếu" class="btn btn-success btn-round">In Phiếu</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var is_on_change = false;
    window.onload = function() {

        change_play_cam_gate_in($('#select_camera_in_test'), 'videoElement_in_test');
        // change_play_cam_gate_in1($('#select_camera_in'), 'videoElement_in');
        change_play_cam_gate_out($('#select_camera_out'), 'videoElement_out');

        // change_play_cam_gate_in($('#select_camera_out'), 'videoElement_in3');
        // change_play_cam_gate_out($('#select_camera_out'), 'videoElement_in4');
        // setInterval(refresh_cam, 60*1000);

        document.addEventListener("visibilitychange", visibilitychange);

    };

    function refresh_cam(){
        change_play_cam_gate_in($('#select_camera_in'), 'videoElement_in');
        change_play_cam_gate_out($('#select_camera_out'), 'videoElement_out');
    }

    function visibilitychange (evt) {
        if(is_on_change) return;
        console.log('refresh22222222222222222222222222');
        is_on_change = true;
        refresh_cam();
        is_on_change = false;
    }
</script>

